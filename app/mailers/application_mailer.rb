class ApplicationMailer < ActionMailer::Base
  default from: 'bon.brannan@gmail.com'
  
  def mailer()
    mail(to: 'bon.brannan@msitec.com', subject: 'sample email')
  end
end
