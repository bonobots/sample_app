module DatabasesHelper
  def datepicker (form, field)
    form.text_field(field, data: {provide: "datepicker",
                                  'date-format': 'dd/mm/yyyy',
                                  'date-autoclose': 'true',
                                  'date-today-btn': 'linked',
                                  'date-today-highlight': 'true'}).html_safe
  end

  def totalTime ()
    total = 0
    @dataview_tags.each do |item|
      clockin = Time.strptime(item[3].to_s.slice(6..7)+":"+item[3].to_s.slice(8..9), '%H:%M').to_i
      clockout = Time.strptime(item[4].to_s.slice(6..7)+":"+item[4].to_s.slice(8..9), '%H:%M').to_i
      clockedtime = clockout - clockin
      total = total + clockedtime
    end
    return Time.at(total).utc.strftime("%H:%M")
  end

  def machinelistJobs ()
    return rand(100)
  end



end