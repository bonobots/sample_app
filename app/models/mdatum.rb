class Mdatum < ApplicationRecord
  include HTTParty
  validates :timeStamp, uniqueness: { case_sensitive: false }
end
