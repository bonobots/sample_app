class Machine < ApplicationRecord
  include HTTParty
  validates :name, uniqueness: { case_sensitive: false }
end
