class DatabasesController < ApplicationController
  include HTTParty
  @@devid = "39a42baa-2057-4a2f-bdd3-7ac2ac8df44e"
  @@username = "admin"
  @@pass = "Msitec123"
  @@account = "US%20Autocure"

  def datatest
    ewonid = 786117
    response = HTTParty.get('https://data.talk2m.com/getstatus?t2maccount='+ @@account +'&t2musername='+ @@username +'&t2mpassword='+ @@pass +'&t2mdevid='+ @@devid +'&id='+ ewonid.to_s)
    @ewon_data = response.parsed_response
  end

  def show
    @mdatas = Mdatum.find(params[:id])
  end

  def machinelist
    response = HTTParty.get('https://data.talk2m.com/getewons?t2maccount='+ @@account +'&t2musername='+ @@username +'&t2mpassword='+ @@pass +'&t2mdevid='+ @@devid, :verify => false)
    @ewon_list = response.parsed_response

    e = Machine.new
    x = *(0..(@ewon_list["ewons"].length-1))
    x.each do |index|
      e.name = @ewon_list["ewons"][index]["name"]
      e.ewonid = @ewon_list["ewons"][index]["id"]
      e.lastsynchrodate = @ewon_list["ewons"][index]["lastSynchroDate"]
      e.save
    end
    @mlist = Machine.all
    @machineListJobs = Mdatum.where(timeStamp: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).count
    @clockings = Mdatum.where(timeStamp: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).pluck(:clockIn, :clockOut)
    @machineListTotalTime = 0
    index = 0
    total = 0
    @clockings.each do
      clockin = Time.strptime(@clockings[index][0].to_s.slice(6..7)+":"+@clockings[index][0].to_s.slice(8..9), '%H:%M').to_i
      clockout = Time.strptime(@clockings[index][1].to_s.slice(6..7)+":"+@clockings[index][1].to_s.slice(8..9), '%H:%M').to_i
      clockedtime = clockout - clockin
      total = total + clockedtime
      index = index + 1
    end
    @machineListTotalTime = Time.at(total).utc.strftime("%H:%M")


  end

  def dataview
    @dataviewname = params[:name]
    ewonid = params[:ewonid]
    @ufromdate = params[:fromDate] 
    @utodate = params[:toDate]

    getewon(ewonid)

    if @ufromdate.nil? or @ufromdate.empty?
      @dataview_tags = Mdatum.where(timeStamp: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day, ewonid: ewonid).pluck(:customerName, :serial, 
        :timeStamp, :clockIn, :clockOut, :cureBaseTime, :cureClearTime,
        :customerID, :tempOverallAvg, :tempSensorAvg1, :tempSensorAvg2, 
        :tempSensorAvg3, :tempSensorAvg4, :tempSensorAvg5, :tempSensorAvg6, 
        :tempSensorAvg7, :tempSensorAvg8, :tempAmbientAvg, :humAmbientAvg)
      @ufromdate = Time.zone.now.strftime("%m/%d/%Y")
    else
      if @utodate.nil? or @utodate.empty?
        @utodate = Time.zone.now.strftime("%m/%d/%Y")
      end
      @dataview_tags = Mdatum.where(timeStamp: Date.strptime(@ufromdate, "%m/%d/%Y").beginning_of_day..Date.strptime(@utodate, "%m/%d/%Y").end_of_day, ewonid: ewonid).pluck(:customerName, :serial, 
        :timeStamp, :clockIn, :clockOut, :cureBaseTime, :cureClearTime,
        :customerID, :tempOverallAvg, :tempSensorAvg1, :tempSensorAvg2, 
        :tempSensorAvg3, :tempSensorAvg4, :tempSensorAvg5, :tempSensorAvg6, 
        :tempSensorAvg7, :tempSensorAvg8, :tempAmbientAvg, :humAmbientAvg)
    end
  end

  def dmpulldown
    m = Machine.all
    m.pluck(:ewonid).each do |id|
      puts id
      getewon(id.to_s)
    end
  end

  def tester 
    puts "bon"
  end

  def getewon(ewonid)
    response = HTTParty.get('https://data.talk2m.com/getewon?t2maccount='+ @@account +'&t2musername='+ @@username +'&t2mpassword='+ @@pass +'&t2mdevid='+ @@devid +'&id='+ewonid, :verify => false)
    @dataview_parsed = response.parsed_response

    e = Mdatum.new
    x = *(0..(@dataview_parsed ["tags"].length-1))
    x.each do |index|
      case @dataview_parsed["tags"][index]["name"]
      when "Customer_Name"
        e.customerName = @dataview_parsed["tags"][index]["value"]
      when "Serial_Number"
        e.serial = @dataview_parsed["tags"][index]["value"]
      when "Time_Stamp"
        timestring = @dataview_parsed["tags"][index]["value"].to_s
        #timestring = DateTime.strptime(timestring, %y%m%d%H%M)
        e.timeStamp = DateTime.strptime(timestring, '%y%m%d%H%M')
      when "Clock_In"
        e.clockIn = @dataview_parsed["tags"][index]["value"]
      when "Clock_Out"
        e.clockOut = @dataview_parsed["tags"][index]["value"]
      when "Cure_Base_Time"
        e.cureBaseTime = @dataview_parsed["tags"][index]["value"]
      when "Cure_Clear_Time"
        e.cureClearTime = @dataview_parsed["tags"][index]["value"]
      when "Customer_ID"
        e.customerID = @dataview_parsed["tags"][index]["value"]
      when "Temp_Overall_Avg"
        e.tempOverallAvg = @dataview_parsed["tags"][index]["value"]
      when "Temp_Sensor_Avg1"
        e.tempSensorAvg1 = @dataview_parsed["tags"][index]["value"]
      when "Temp_Sensor_Avg2"
        e.tempSensorAvg2 = @dataview_parsed["tags"][index]["value"]
      when "Temp_Sensor_Avg3"
        e.tempSensorAvg3 = @dataview_parsed["tags"][index]["value"]
      when "Temp_Sensor_Avg4"
        e.tempSensorAvg4 = @dataview_parsed["tags"][index]["value"]
      when "Temp_Sensor_Avg5"
        e.tempSensorAvg5 = @dataview_parsed["tags"][index]["value"]
      when "Temp_Sensor_Avg6"
        e.tempSensorAvg6 = @dataview_parsed["tags"][index]["value"]
      when "Temp_Sensor_Avg7"
        e.tempSensorAvg7 = @dataview_parsed["tags"][index]["value"]
      when "Temp_Sensor_Avg8"
        e.tempSensorAvg8 = @dataview_parsed["tags"][index]["value"]
      when "Temp_Ambient_Avg"
        e.tempAmbientAvg = @dataview_parsed["tags"][index]["value"]
      when "Hum_Ambient_Avg"
        e.humAmbientAvg = @dataview_parsed["tags"][index]["value"]
      end
    end
    e.ewonid = @dataview_parsed["id"]
    e.save
  end

  def emailsent()
    ApplicationMailer.mailer().deliver
  end

  def dailyreport()
    @dailytags = Mdatum.where(timeStamp: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).pluck( 
      :timeStamp, :clockIn, :clockOut, :cureBaseTime, :cureClearTime,
      :tempOverallAvg, :tempSensorAvg1, :tempSensorAvg2, 
      :tempSensorAvg3, :tempSensorAvg4, :tempSensorAvg5, :tempSensorAvg6, 
      :tempSensorAvg7, :tempSensorAvg8, :tempAmbientAvg, :humAmbientAvg)

    respond_to do |format|
      format.xlsx {
        response.headers[
          'Content-Disposition'
        ] = "attachment; filename = 'dailyreport.xlsx'"
      }
      format.html {render :dailyreport }
    end
  end

end
