# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190530005850) do

  create_table "machines", force: :cascade do |t|
    t.string "name"
    t.integer "ewonid"
    t.datetime "lastsynchrodate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mdata", force: :cascade do |t|
    t.string "customerName"
    t.string "serial"
    t.datetime "timeStamp"
    t.integer "clockIn"
    t.integer "clockOut"
    t.integer "cureBaseTime"
    t.integer "cureClearTime"
    t.integer "customerID"
    t.integer "tempOverallAvg"
    t.integer "tempSensorAvg1"
    t.integer "tempSensorAvg2"
    t.integer "tempSensorAvg3"
    t.integer "tempSensorAvg4"
    t.integer "tempSensorAvg5"
    t.integer "tempSensorAvg6"
    t.integer "tempSensorAvg7"
    t.integer "tempSensorAvg8"
    t.integer "tempAmbientAvg"
    t.integer "humAmbientAvg"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "ewonid"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
