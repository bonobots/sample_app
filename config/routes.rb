Rails.application.routes.draw do
  get 'sessions/new'

  get 'sessions/create'

  get 'sessions/destroy'

  get 'users/new'

  root 'static_pages#home'
  get '/help',        to: 'static_pages#help'
  get '/about',       to: 'static_pages#about'
  get '/contact',     to: 'static_pages#contact'
  get '/datatest',    to: 'databases#datatest'
  get '/machinelist', to: 'databases#machinelist'
  get '/dataview',    to: 'databases#dataview'
  get '/emailsent',   to: 'databases#emailsent'
  get '/dailyreport', to: 'databases#dailyreport'

  resources :users
  resources :sessions, only: [:new, :create, :destroy]

  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'

end
